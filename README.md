<h1 align="center"><a href="https://github.com/KartSriv/Story/"><img src="https://image.flaticon.com/icons/svg/1602/1602750.svg" alt="Logo" width="200"></a><h1 align="center">Story</h1></h1><h2 align="center">The world's first <a href="https://en.wikipedia.org/wiki/Open-source_software" target="https://github.com/KartSriv/Story/">Open Source</a> Story.</h2></a>
  <a href="https://saythanks.io/to/KartSriv"><img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg"> </a> <a href="https://devrant.com/rants/2057082"><img src="https://img.shields.io/badge/story%20status-interesting-blue.svg"></a>
</p>

[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://github.com/KartSriv/)
[![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://github.com/KartSriv/)

## Before Writing

<b>You can get started by reading the <a href="https://github.com/KartSriv/Story/blob/master/CONTRIBUTING.md">Contribution Guidelines.</a><br></b>
<b>Also, please try to keep up with the <a href="https://github.com/KartSriv/Story/blob/master/CODE_OF_CONDUCT.md">Code of Conduct</a></b>

**Start writing in the [story directory](Story).** <br>
**Remember. Do not start writing on chapters already completed. 
Write on the latest one.**

## Contributors
**Repository owner**
[KartSriv:](https://github.com/KartSriv)

**Collaborators**
[HampusM:](https://github.com/HampusM)

# The story
## Chapter 1


Once upon a time, there was a family of marsupials.
They lived in Downtown LA and have never traveled outside of this neighborhood before but this all changed when a celestial object fell on their backyard, this accident changed their lives forever. But who are these family of marsupials, why have they never left Downtown LA and why should we care? To answer that we need to know what happened 6 years before the accident.     
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6 years back, there was no house. Then were did they live? Well, they werent a family back then. That means that they all lived at different places. Tom, the father in the family lived in a small apartment in Boston with his grandparents.

