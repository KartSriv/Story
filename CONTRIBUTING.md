# Contributing to our story

Thank you for considering contributing to the story. 
We gladly welcome new contributions to our mutual story and we hope that you will enjoy it as well as we do.

## Getting started
Getting started with contributing doesn't take much.

**Adding to the story**
1. Read the [Code Of Conduct](CODE_OF_CONDUCT.md)
2. Create a fork
3. Make your changes on the fork you've created
3. Create a pull request with the appropriate template. There is one template for a small change and one for a big change.

**Requesting that something get's changed/added**<br>
If you're too lazy to actually do something yourself, you can always create a issue and describe what you want too be changed or added to the story.

**Changing something else**
For changing or adding something in anything other than the story, just make a issue or pull request like normal with the appropriate template. 

## Rules
* The change must be well written and have a red line
* The change must follow the [Code Of Conduct](CODE_OF_CONDUCT)
* Do not add a character or another storyline unless it's completely necessary
* Keep the characters with their original personalities. Characters personalities can change as the story progress
* If you're going to make a change or add something to a already completed chapter, make sure it makes sense in the context of everything after it. Any major changes in a completed chapter won't get accepted

## How to get your pull request/ issue accepted
A pull request for the story will be accepted if it
1. Follows the [rules](#Rules)
2. It contains content that progresses the story in any way

A pull request that changes anything else (for example the [readme](README.md) or this document) will be accepted by normal standards.
